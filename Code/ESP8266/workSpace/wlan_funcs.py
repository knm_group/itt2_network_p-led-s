import network

def web_page(wlan, temp, humidity):
  wlan_scan = wlan.scan()
  scan_entry = ""
  for i in wlan_scan:
    scan_entry += "<br>" + str(i)
  html = ("""<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head>""" + """
  <body><h1>Scan results:</h1>""" + """<p>""" + scan_entry + """</p>""" + """
  <h2>Temp & Humidity</h2>""" + """<p>""" +temp + """<br>"""+ humidity + """</p>""" + """</body></html>""")
  return html

def do_connect(SSID, PASSWORD):
  wlan = network.WLAN(network.STA_IF)
  wlan.active(True)
  print(wlan.scan())
  if not wlan.isconnected():
      print('connecting to network...')
      wlan.connect(SSID, PASSWORD)
      while not wlan.isconnected():
          pass
  print('network config:', wlan.ifconfig())
  return wlan
  
def webserver_html(html,socket):
  print("Looking for connection")
  conn, addr = socket.accept()
  print('Got a connection from %s' % str(addr))
  request = conn.recv(1024)
  print('Content = %s' % str(request))
  response = html
  conn.send('HTTP/1.1 200 OK\n')
  conn.send('Content-Type: text/html\n')
  conn.send('Connection: close\n\n')
  conn.sendall(response)
  conn.close()







