import socket
import network
import dht
from time import sleep
import machine
from machine import Pin, ADC
from wlan_funcs import web_page, do_connect, webserver_html
from pin_funcs import APA102
from mqtt_github_lib import MQTTClient
import ubinascii
import micropython
import gc  # Garbage-collector for micropython (from Cpython)

LEDS = APA102(LED_COUNT=18, Brightness=255, sck=14, miso=12,
              mosi=13, status_LED_count=3)

light_value_cutoff_active = 0



def main_func():
    gc.collect()
    topic_light_switch = b'system/light_switch'
    topic_LED_colour = b'system/LED_colour'
    topic_light_cutoff = b'system/photoresistor'
    topic_publish_humidity = b'system/humidity'
    topic_publish_temperature = b'system/temperature'
    topic_publish_moisture = b'system/moisture'
    
    light_value_cutoff = 800
    #photoresistor value should be around 800 for cut-off in dimm light imo (with 270 res after)
    SSID = "34kl"
    PASSWORD = "ty47glr8"
    MQTT_broker_address = "192.168.43.113"
    client_id = ubinascii.hexlify(machine.unique_id())
    dht_pin = 15
    soil_power_pin = 0
    photoresistor_power_pin = 16
    LED_power_pin = 2
    
    global LED_power
    LED_power = Pin(LED_power_pin, Pin.OUT, value=1)
    button_5 =Pin(5, Pin.IN, Pin.PULL_UP)
    print("press Button to access")
    sleep(4)
    if button_5.value() == 0:
        print("Access granted")
        return
    
    LEDS.change_plant_LED_colour(255, 0, 100)
    
    LEDS.boot_LED(1)

    soil_power = Pin(soil_power_pin, Pin.OUT, value=0)
    photoresistor_power = Pin(photoresistor_power_pin, Pin.OUT, value=0)

    DHT_sensor = dht.DHT22(Pin(dht_pin))
    DHT_sensor.measure()
    temp = str(DHT_sensor.temperature())
    humidity = str(DHT_sensor.humidity())
    
    
    
    wlan = do_connect(SSID, PASSWORD)
    #ESP32_IP = wlan.ifconfig()[0]
    
    LEDS.boot_LED(2)
    
    mqtt_client = mqtt_client_connect(client_id, MQTT_broker_address)
    mqtt_client.subscribe(topic_LED_colour)
    mqtt_client.subscribe(topic_light_cutoff)
    mqtt_client.subscribe(topic_light_switch)
 
    LEDS.boot_LED(3)
    
    mqtt_client.check_msg()
    global active_flag
    active_flag = 1
    
    while(button_5.value() == 1):
        print("in loop")
        moisture = read_from_adc(soil_power) #reads adc-value on soil-pin
        light_value = read_from_adc(photoresistor_power) #reads adc-value on photoresistor-pin
        print(light_value)
        print(moisture)
        LED_state = LED_power.value()
        if light_value_cutoff_active:
            if ((light_value > light_value_cutoff) & LED_state):
                LED_power.off()
                active_flag = 0
            elif (light_value < light_value_cutoff):# & (not LED_state):
                turn_on_LEDs()
                active_flag = 1
        else:
            if active_flag == 0:
                turn_on_LEDs()
                active_flag = 1
            else:
                pass
        
        DHT_sensor.measure()
        temp = str(DHT_sensor.temperature())
        humidity = str(DHT_sensor.humidity())
        
        mqtt_client.publish(topic_publish_humidity, str(humidity))
        mqtt_client.publish(topic_publish_moisture, str(moisture))
        mqtt_client.publish(topic_publish_temperature, str(temp))
        
        sleep(2.5)
        
        mqtt_client.check_msg()
    
    print("loop broken")
    
    wlan.disconnect()

    return
    
def turn_on_LEDs():
    LED_power.on()
    sleep(0.1)
    LEDS.LED_update()

def sub_callback(topic, msg):
    print((topic, msg))
    topic = topic.split(b'/')
    print(topic[-1])
    MQTT_msg_funcs[topic[-1]](msg)
    return
  
  
def MQTT_LED_change(msg):
    colours = [msg.decode()[i:i+2] for i in range(0, len(msg.decode()), 2)]
    Red_value, Green_value, Blue_value = colours
    print(int(Blue_value, 16), int(Green_value, 16), int(Red_value, 16))
    LEDS.change_plant_LED_colour(int(Blue_value, 16), int(Green_value, 16), int(Red_value, 16))

def mqtt_client_connect(client_id, MQTT_broker_address):
    mqtt_client = MQTTClient(client_id, server=MQTT_broker_address)
    mqtt_client.set_callback(sub_callback)
    mqtt_client.connect()
    return mqtt_client

def read_from_adc(power_pin):
    power_pin.on()
    sleep(0.3)
    read_value = ADC(0).read()
    power_pin.off()
    return read_value
    
def light_value_cutoff_flagset(msg):
    global light_value_cutoff_active
    light_value_cutoff_active = int(msg)
    
def light_switch(msg):
    if int(msg) == 1:
      turn_on_LEDs()
      
    else:
      global active_flag
      active_flag = 1
      LED_power.off()
      
MQTT_msg_funcs = {
    b'LED_colour':MQTT_LED_change,
    b'photoresistor':light_value_cutoff_flagset,
    b'light_switch':light_switch,
}

main_func()




