import socket
import network
import dht
from time import sleep
import machine
from machine import Pin, ADC
from wlan_funcs import web_page, do_connect, webserver_html, find_mqtt_server
from pin_funcs import APA102
from mqtt_github_lib import MQTTClient
import ubinascii
import micropython
import gc  # Garbage-collector for micropython (from Cpython)

LEDS = APA102(LED_COUNT=5, Brightness=225, sck=14, miso=12,
              mosi=13, status_LED_count=4)

light_value_cutoff_active = 0

#photoresistor value should be around 2500 for cutoff in dimm light imo (with 10k res after)

def main_func():
    gc.collect()
    topic_sub = b'system/LED_colour'
    topic_light_cutoff = b'system/photoresistor'
    topic_publish_humidity = b'system/humidity'
    topic_publish_temperature = b'system/temperature'
    topic_publish_moisture = b'system/moisture'
    
    light_value_cutoff = 2500
    SSID = "34kl"
    PASSWORD = "ty47glr8"
    mqtt_server_name = "Mqtt-pi.local"
    client_id = ubinascii.hexlify(machine.unique_id())
    dht_pin = 27
    photoresistor_pin = 34
    soil_pin = 39
    LED_power_pin = 25
    
    soil_power_pin = 4
    LED_power = Pin(LED_power_pin, Pin.OUT, value=1)
    button_0 =Pin(0, Pin.IN, Pin.PULL_UP)
    print("press Button to access")
    sleep(4)
    if button_0.value() == 0:
        print("Access granted")
        return
    
    LEDS.change_plant_LED_colour(255, 0, 100)
    
    LEDS.boot_LED(1)

    photoresistor_adc = setup_adc_inputs(photoresistor_pin)
    soil_adc = setup_adc_inputs(soil_pin)
    soil_power = Pin(soil_power_pin, Pin.OUT, value=0)
    print(read_from_soil(soil_adc, soil_power)) #prints adc-value on soil-pin
    print(photoresistor_adc.read()) #prints adc-value on photoresistor-pin

    DHT_sensor = dht.DHT22(Pin(dht_pin))
    DHT_sensor.measure()
    temp = str(DHT_sensor.temperature())
    humidity = str(DHT_sensor.humidity())
    
    LEDS.boot_LED(2)
    
    wlan = do_connect(SSID, PASSWORD)
    ESP32_IP = wlan.ifconfig()[0]
    
    LEDS.boot_LED(3)
    
    MQTT_broker_address = find_mqtt_server(ESP32_IP, \
                          client_id, mqtt_server_name)
    
    mqtt_client = mqtt_client_connect(client_id, MQTT_broker_address)
    mqtt_client.subscribe(topic_sub)
    mqtt_client.subscribe(topic_light_cutoff)
    
    LEDS.boot_LED(4)
    
    mqtt_client.check_msg()
    
    while(button_0.value() == 1):
        print("in loop")
        moisture = read_from_soil(soil_adc, soil_power) #reads adc-value on soil-pin
        light_value = photoresistor_adc.read() #reads adc-value on photoresistor-pin
        if (light_value > light_value_cutoff) & light_value_cutoff_active:
            LED_power.off()
        else:
            LED_power.on()
        
        DHT_sensor.measure()
        temp = str(DHT_sensor.temperature())
        humidity = str(DHT_sensor.humidity())
        
        mqtt_client.publish(topic_publish_humidity, str(humidity))
        mqtt_client.publish(topic_publish_moisture, str(moisture))
        mqtt_client.publish(topic_publish_temperature, str(temp))
        
        sleep(1)
        
        mqtt_client.check_msg()
    
    print("loop broken")
    
    wlan.disconnect()

    return
    


def sub_callback(topic, msg):
    print((topic, msg))
    topic = topic.split(b'\\')
    print(topic[-1])
    MQTT_msg_funcs[topic[-1]](msg)
    return
  
  
def MQTT_LED_change(msg):
    colours = msg.split(b'.')
    Blue_value, Green_value, Red_value = colours
    print(int(Blue_value), int(Green_value), int(Red_value))
    LEDS.change_plant_LED_colour(int(Blue_value), int(Green_value), int(Red_value))

def mqtt_client_connect(client_id, MQTT_broker_address):
    mqtt_client = MQTTClient(client_id, server=MQTT_broker_address)
    mqtt_client.set_callback(sub_callback)
    mqtt_client.connect()
    return mqtt_client

def setup_adc_inputs(adc_pin):
    print("configuring adc inputs")
    new_adc = ADC(Pin(adc_pin))
    new_adc.atten(ADC.ATTN_11DB) #Sets voltage range to 3.6V max
    return new_adc

def read_from_soil(soil_adc, soil_power):
    soil_power.on()
    sleep(1)
    soil_value = soil_adc.read()
    soil_power.off()
    return soil_value
    
def light_value_cutoff_flagset(msg):
    light_value_cutoff = int(msg)

MQTT_msg_funcs = {
    b'LED_colour':MQTT_LED_change,
    b'photoresistor': light_value_cutoff_flagset,
}

main_func()








