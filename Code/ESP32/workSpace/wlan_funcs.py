import network
import slimDNS

def web_page(wlan, temp, humidity):
  wlan_scan = wlan.scan()
  scan_entry = ""
  for i in wlan_scan:
    scan_entry += "<br>" + str(i)
  html = ("""<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head>""" + """
  <body><h1>Scan results:</h1>""" + """<p>""" + scan_entry + """</p>""" + """
  <h2>Temp & Humidity</h2>""" + """<p>""" +temp + """<br>"""+ humidity + """</p>""" + """</body></html>""")
  return html

def do_connect(SSID, PASSWORD):
  wlan = network.WLAN(network.STA_IF)
  wlan.active(True)
  print(wlan.scan())
  if not wlan.isconnected():
      print('connecting to network...')
      wlan.connect(SSID, PASSWORD)
      while not wlan.isconnected():
          pass
  print('network config:', wlan.ifconfig())
  return wlan
  
def webserver_html(html,socket):
  print("Looking for connection")
  conn, addr = socket.accept()
  print('Got a connection from %s' % str(addr))
  request = conn.recv(1024)
  print('Content = %s' % str(request))
  response = html
  conn.send('HTTP/1.1 200 OK\n')
  conn.send('Content-Type: text/html\n')
  conn.send('Connection: close\n\n')
  conn.sendall(response)
  conn.close()

def find_mqtt_server(ESP32_IP, client_id, mqtt_server_name):
  slimDNS_server_obj = slimDNS.SlimDNSServer(ESP32_IP, str(client_id))
  mem_view_adress = memoryview(slimDNS_server_obj.resolve_mdns_address(mqtt_server_name))
  mqtt_server_address_array = []
  for i in mem_view_adress:
    print(str(mem_view_adress))
    mqtt_server_address_array.append(str(i))
  MQTT_broker_address = ".".join(mqtt_server_address_array)
  print(type(MQTT_broker_address))
  return MQTT_broker_address





