from machine import Pin, SPI


class APA102:
  """Class for APA102 in mcpython with support
     for Status-LEDs & dedicated plant-LEDs"""
  Brightness = [0b11100001]  # 225
  LED_COUNT = 0
  LED_matrix = []
  spi_port = "spi"
  startframe = [0b00000000]*4
  endframe = [0b11111111]*4
  status_LED_count = 4
  Single_LED_BRG_values = [0b00000000]*4
  Boot_Blue_value = 0
  Boot_Green_value = 255
  Boot_Red_value = 0

  def __init__(self, LED_COUNT, status_LED_count, Brightness,
               sck, mosi, miso):
    self.plant_LEDs = LED_COUNT - status_LED_count
    self.Brightness = Brightness
    self.Single_LED_BRG_values[0] = Brightness
    self.LED_COUNT = LED_COUNT
    self.LED_matrix = [None] * LED_COUNT
    self.status_LED_count = status_LED_count
    #
    for i in range(LED_COUNT):
      self.LED_matrix[i] = self.Single_LED_BRG_values
    self.setup_LED_pins(sck, mosi, miso)

  def setup_LED_pins(self, sck_pin, mosi_pin, miso_pin):
    self.spi_port = SPI(1, 10000000, sck=Pin(sck_pin),
                    mosi=Pin(mosi_pin), miso=Pin(miso_pin))

  def send_startframe(self):
    self.spi_port.write(bytearray(self.startframe))

  def send_endframe(self):
    self.spi_port.write(bytearray(self.endframe))

  def send_colour(self, amount):
    for i in range(amount):
      self.spi_port.write(bytearray(self.LED_matrix[i]))

  def LED_update(self):
    self.send_startframe()
    self.send_colour(self.LED_COUNT)
    self.send_endframe()

  def change_status_LED_colour(self, Blue_value, Green_value, Red_value):
    for i in range(self.status_LED_count):
      self.LED_matrix[i] = [self.Brightness, Blue_value,
                          Green_value, Red_value]

  def change_plant_LED_colour(self, Blue_value, Green_value, Red_value):
    for i in range(self.status_LED_count, self.LED_COUNT):
      self.LED_matrix[i] = [self.Brightness, Blue_value,
                          Green_value, Red_value]
    self.LED_update()

  def boot_LED(self, boot_state):
    for i in range(boot_state):
      self.LED_matrix[i] = [self.Brightness, self.Boot_Blue_value,
                          self.Boot_Green_value, self.Boot_Red_value]
    self.LED_update()


