## https://github.com/bsmaat/mqtt/blob/master/sql/sqlwriter.py ##

from time import gmtime, strftime
import paho.mqtt.client as mqtt
import sqlite3

temperature_topic = "system/temperature"
humidity_topic = "system/humidity"
moisture_topic = "system/moisture"
dbFile = "data.db"

dataTuple = [-1, -1, -1]

conn = sqlite3.connect(dbFile)
c = conn.cursor()
# create table
try:
    c.execute('''CREATE TABLE climate
                 (date text, temperature real, humidity real, moisture real)''')
    conn.commit()
except:
    print("Table probably already exists")


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(temperature_topic)
    client.subscribe(humidity_topic)
    client.subscribe(moisture_topic)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global dataTuple
    theTime = strftime("%Y-%m-%d %H:%M:%S", gmtime())

    result = (theTime + "\t" + str(msg.payload))
    print(msg.topic + ":\t" + result)
    if (msg.topic == temperature_topic):
        dataTuple[0] = str(msg.payload)
    if (msg.topic == humidity_topic):
        dataTuple[1] = str(msg.payload)
    if (msg.topic == moisture_topic):
        dataTuple[2] = str(msg.payload)
    if (dataTuple[0] != -1 and dataTuple[1] != -1 and dataTuple[2] != -1):
        writeToDb(theTime, dataTuple[0], dataTuple[1], dataTuple[2])
    return


def writeToDb(theTime, temperature, humidity, moisture):
    print("Writing to db...")
    c.execute("INSERT INTO climate VALUES (?,?,?,?)", (theTime, temperature,
              humidity, moisture))
    conn.commit()

    global dataTuple
    dataTuple = [-1, -1, -1]


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("Mqtt-pi.local", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
