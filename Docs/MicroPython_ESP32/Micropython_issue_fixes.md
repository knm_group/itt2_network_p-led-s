# Wifi

## Connection not possible on second reset with reset-button
When connecting to an access point after already having connected before the reset the ESP32 is caught in a loop when calling the connect function.
This seems to be a problem created by the network libary trying to authenticate when connecting to an AP that it was connected to before. 
This leads the authentication to fail causing the connect-method to hang, thereby stopping the rest of the code from running.

Workaround for now would be to add a delay when booting to keep the authentication from failing by letting the connection be forgotton by the AP.
If possible a function should be written to allow calling the sub-routines of the connect method sepratly to create a reconnect method instead.
