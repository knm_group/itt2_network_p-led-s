1.  install/flash Raspbian stretch lite


2. Connect to WiFi & Enable SSH
	https://medium.com/@danidudas/install-raspbian-jessie-lite-and-setup-wi-fi-without-access-to-command-line-or-using-the-network-97f065af722e

3. setup localisation (Dunno if needed)
	`sudo raspi-config`

	4. Localisation Options
		I1 Change Locale

        I2 Change Timezone

        I3 Change Keyboard Layout

        I4 Change Wi-Fi Country

4. Update & Upgrade (Optional)

   `sudo apt -qq update && sudo apt -qq upgrade -y && sudo apt -qq full-upgrade -y`

5. AskSensors Tutorial "kinda" followed 

   https://www.instructables.com/id/Connect-Raspberry-Pi-to-Asksensors-IoT-Platform-Us/

   Before installing create AskSensors.com account and create a sensor

   https://www.youtube.com/embed/NaeDetqQtW0?feature=oembed

6. Create test project directory on the pi

   `mkdir AskSensors-TestCode`

   `cd AskSensors-TestCode`

7.  Install Node.js

   `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`

   `sudo apt-get -q install -y nodejs`


8. Verify installation (optional)

   `node -v`

   v10.15.3

   `npm -v`

   6.4.1 


9. Install https package

   npm i https

10. Install npm request package

    npm i request

11. Download example code from https://github.com/asksensors/AskSensors-node.js-API

    curl -LJO https://raw.githubusercontent.com/asksensors/AskSensors-node.js-API/master/https_get.js

12.  Insert API key from asksensors.com and edit interval in Miliseconds in **https_get.js**

    var ApiKeyIn = '................'; // Api Key In
    var timerInterval = 20000;        // timer interval

13. Start sending demo data

    node https_get.js

14. Go to asksensors.com and playaround with modules and graphs

    https://www.youtube.com/embed/NaeDetqQtW0?feature=oembed