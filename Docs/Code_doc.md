# ESP32 Micropython

## Table of Contents

- [boot.py](#boot-py)
- [main.py](#main-py)
- [wlan_funcs.py](#wlan_funcs-py)
- [pin_funcs.py](#pin_funcs-py)

## boot.py
This file is excecuted first and can be used for system-configuration like setting the debug-level.

## main.py 
This file is as default executed after the boot.py file and is where all functions should be called from.
__WARNING__: Don't define a "main"-function as that will cause the ESP32 to fault on executing it.

## wlan_funcs.py
This file contains the wlan-related function including any data-transfer over it.

Needs network module to work.

| *Function-name* | *Function* | *Args* | *Returns* |
| --------------- | ---------- | ------ | --------- |
| web_page | Returns html including all wlan in the area | wlan | html |
| do_connect | Connects to an AP specified by the SSID | SSID , PASSWORD | wlan |
| webserver_html | Waits for a socket connection and sends the html over it | html, socket | 

## pin_funcs.py
Contains SPI-related functions.

Needs PIN & SPI from machine module to function.

### APA102

Initializeing needs the following keywords:
LED_count, status_LED_count, Brightness, sck, mosi, miso

| *Function-name* | *Function* | *Args* | *Returns* |
| --------------- | ---------- | ------ | --------- |
| setup_LED_pins | sets up the hspi hardware pins | sck_pin, mosi_pin, miso_pin | N/A |
| send_startframe_SPI | sends the startframe specified by the APA102's documentation | N/A | N/A |
| send_endframe_SPI | sends the endframe specified by the APA102's documentation | N/A | N/A |
| send_colour | sends the colour-configuration over the SPI port specified in the setup_LED_pins function for all LEDs in the array until the specified number| amount | N/A |
| LED_update | updates LED with the values included in the LED matrix in the range until the parsed LED_count | N/A | N/A |
| change_status_LED_colur | Changes the colour of the status LEDs in the LED matrix to the parsed colour without updating the LEDs | Blue_value, Green_value, Red_value |
| change_plant_LED_colour | Changes the plant related arrays of the LED matrix to the parsed colours without updating the LEDs | Blue_value, Green_value, Red_value | N/A |
| boot_LED | Function for the Startup ticking on the Status-LEDs, is called with amounts ranging from 1-4 to update the corresponding amount of LEDs with the static startup colour | boot_state | LED_BRG_values |
