# APA102
[https://cdn-shop.adafruit.com/datasheets/APA102.pdf](https://cdn-shop.adafruit.com/datasheets/APA102.pdf)

# ESP32 Wroom
[https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf](https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf)

ADARFRUIT Ting plus: [https://learn.sparkfun.com/tutorials/esp32-thing-plus-hookup-guide](https://learn.sparkfun.com/tutorials/esp32-thing-plus-hookup-guide)

# ESP8266 Pin-Diagram
Pin-diagram: [https://learn.adafruit.com/assets/31354](https://learn.adafruit.com/assets/31354)

Adafruit tutorial: [https://learn.adafruit.com/adafruit-feather-huzzah-esp8266/pinouts](https://learn.adafruit.com/adafruit-feather-huzzah-esp8266/pinouts)

# Photresistor
[https://cdn.sparkfun.com/datasheets/Sensors/LightImaging/SEN-09088.pdf](https://cdn.sparkfun.com/datasheets/Sensors/LightImaging/SEN-09088.pdf)

# DHT22
[https://cdn-shop.adafruit.com/datasheets/DHT22.pdf](https://cdn-shop.adafruit.com/datasheets/DHT22.pdf)

# Soil-moisture sensor
[https://learn.sparkfun.com/tutorials/soil-moisture-sensor-hookup-guide](https://learn.sparkfun.com/tutorials/soil-moisture-sensor-hookup-guide)
