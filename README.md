# Plant LED monitoring System

The main purpose of this system will be to allow a monitoring of a plant via the system's built-in sensors and allow acces to that Data through a web-interface on a client, while being able to control LEDs connected to the system to allow individual lighting for the palnt.

Communication is be done via MQTT between a Rasperry Pi broker and an ESP32 on the system.


The measured variables are:

* Air-moistiure
* Soil-moisture
* Temperature
* Light-level

It is expected that MicroPython will be used for programming the ESP32, while Python will be used for all code on the Raspberry Pi.

The MQTT broker used is Moquitto,

[https://mosquitto.org/](https://mosquitto.org/)


# Installation of system

Run the ReintallationScript (found in /Installation) on the raspberry pi to set it up.

Flash the code contained in the folder /Code/ESP32/workSpace onto the ESP32. Use something alike uPyCraft.

