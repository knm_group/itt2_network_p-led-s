#!/bin/bash

## run with sudo ./"nameOfScript".sh



#Link for downloading the Project
#project_zip_link="https://gitlab.com/theonlyfp/2_sem_proj_knm/-/archive/master/2_sem_proj_knm-master.zip"
#echo $project_zip_link

#Name of project
project_name="itt2_network_p-led-s-master"

#Path to flask
#flask_api_path="/Flask_api/flask_menu.py"

#Which gitlab-accounts are allowed connection
Allowed_users=(kenbje NicolaiLyngs theonlyfp)

#Networking settings
#Interface="eth0"
#Pi_ip="172.20.10.3/24"
#Router_ip="172.20.10.1"
#DNS_ip="10.140.12.3"

echo Executing the Installation script, please hang on
echo Enabling SSH

#enable SSH
sudo systemctl enable ssh
sudo systemctl start ssh

#Add network config for static IP
#echo Configuring static IP
#echo "" >> /etc/dhcpcd.conf
#echo "interface $Interface" >> /etc/dhcpcd.conf
#echo "static ip_address=$Pi_ip" >> /etc/dhcpcd.conf
#echo "static routers=$Router_ip" >> /etc/dhcpcd.conf
#echo "static domain_name_servers=$DNS_pi" >> /etc/dhcpcd.conf

#Enable Serial ports
#echo Enabling serial ports
#echo "" >> /boot/config.txt
#echo "enable_uart=1" >> /boot/config.txt




#Add user's SSH-keys
echo ${Allowed_users[*]}

sudo mkdir ~/.ssh

touch ~/.ssh/authorized_keys

echo "" > ~/.ssh/authorized_keys

for user in ${Allowed_users[*]}
do
    echo item: Adding User: $user
    curl https://gitlab.com/$user.keys -w "\n" >> ~/.ssh/authorized_keys
done

#update / upgrade
sudo apt-get update
sudo apt-get upgrade

#install software
sudo apt install -y -q mosquitto mosquitto-clients
#Install Node-red + Dashboard
echo y | bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)
sudo systemctl enable nodered.service
cd ~/.node-red/
npm install node-red-dashboard
npm install node-red-node-mysql
npm install node-red-node-sqlite
#sudo apt-get -y -q install socat
sudo apt-get -y -q install python3-pip
pip3 install paho-mqtt
cd ~
#sudo pip3 install pyserial -q
#pip3 install flask -q
#pip3 install flask_restful -q
#pip3 install flask_cors -q
mkdir ~/AskSensors-TestCode && cd AskSensors-TestCode
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get -q install -y nodejs
npm i https requests

cd ~




#Get zip-file
#wget -O $project_name.zip $project_zip_link

#unzip file
#unzip -o $project_name.zip #Unpacks into folder called the same as project_name

#delete zip-file
#sudo rm $project_name.zip

# make flask auto-run @reboot
#echo "[Unit]" > /lib/systemd/system/flask_api.service
#echo "Description=flask api service" >> /lib/systemd/system/flask_api.service
#echo "After=network.target" >> /lib/systemd/system/flask_api.service

#echo "[Service]" >> /lib/systemd/system/flask_api.service
#echo "ExecStart=/usr/bin/python3 /home/pi/"$project_name$flask_api_path >> /lib/systemd/system/flask_api.service

#echo "[Install]" >> /lib/systemd/system/flask_api.service
#echo "WantedBy=multi-user.target" >> /lib/systemd/system/flask_api.service

#sudo chmod 644 /lib/systemd/system/flask_api.service

#sudo systemctl daemon-reload
#sudo systemctl enable flask_api.service


#execute testfiles
cd $project_name
cd test
#socat -d -d pty,raw,echo=0,link=./fake_atmega pty,raw,echo=0,link=client &
#chmod 755 ./fake_atmega.py
#python3 ./fake_atmega.py &
#cd ..
#python3 simple_connection.py
echo "Everything was set-up sucessfully (I do hope)"
echo "Rebooting now"
jobs -p | xargs kill

sleep 4s

sudo reboot
